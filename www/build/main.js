webpackJsonp([1],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__home_home__ = __webpack_require__(152);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InfoPage = /** @class */ (function () {
    function InfoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.isGo = false;
    }
    InfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InfoPage');
    };
    InfoPage.prototype.go = function () {
        var _this = this;
        if (!this.isGo) {
            this.isGo = true;
            setTimeout(function () {
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__home_home__["a" /* HomePage */]).then(function () {
                    _this.isGo = false;
                });
            }, 300);
        }
    };
    InfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-info',template:/*ion-inline-start:"/home/moce/Desktop/bon/DCH/src/pages/info/info.html"*/'<ion-content class="content">\n    <div class="con">\n        <img src="assets/imgs/xxd.svg" alt="">\n    </div>\n    <div class="to-bottom">\n        <div [class]="isGo?\'btn-b active\':\'btn-b\'" (click)="go()">\n            <div>ต่อไป</div>\n        </div>\n    </div>\n</ion-content>\n'/*ion-inline-end:"/home/moce/Desktop/bon/DCH/src/pages/info/info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* NavParams */]])
    ], InfoPage);
    return InfoPage;
}());

//# sourceMappingURL=info.js.map

/***/ }),

/***/ 110:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 110;

/***/ }),

/***/ 151:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/info/info.module": [
		287,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 151;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__capacitor_core__ = __webpack_require__(248);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var Geolocation = __WEBPACK_IMPORTED_MODULE_2__capacitor_core__["a" /* Plugins */].Geolocation;
var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.show = 0;
        this.showMenu = false;
        this.loading = false;
        this.isClose = true;
        this.directionsService = new google.maps.DirectionsService;
        this.directionsDisplay = new google.maps.DirectionsRenderer;
        this.data = [
            {
                number: 1,
                title: "เก๋ากี๋",
                image: "assets/imgs/556000008782801.JPEG",
                detail: "หลายคนคงเคยพบเห็นสมุนไพรตัวนี้ เพราะเป็นสมุนไพรที่ค่อนข้างพบเจอได้ง่ายโดยเฉพาะในต้มซุป ต้มจืด หรือประเภทตุ๋นต่างๆ เก๋ากี้ ก็คือ ผลโกจิเบอร์รี ที่มีแหล่งที่มาจากเทือกเขาหิมาลัย มีกลิ่นหอม ส่วนรสชาตินั้นดีทีเดียว อีกทั้งยังมีสรรพคุณต่อสุขภาพร่างกายอีกด้วย เก๋ากี้อุดมไปด้วยวิตามินเอ, ซีและสารต้านอนุมูลอิสระกลุ่มแคโรทีนอยด์ อาทิ เบต้าแคโรทีนที่มีปริมาณค่อนข้างสูงเลยทีเดียว ส่วนเรื่องสรรพคุณนั้นมีส่วนช่วยชะลอความแก่ เพราะมีสารแอนติออกซิแดนต์สูงมาก, ช่วยเรื่องผิวพรรณ, มีคุณสมบัติต้านมะเร็ง, ลดระดับน้ำตาลในเลือด, ลดไขมันในเลือด อีกทั้งยังป้องกันตับไม่ให้มีไขมัน, บำรุงสายตา ทำให้การมองเห็นชัดเจนขึ้น ที่สำคัญมีส่วนช่วยเพิ่มฮอร์โมนเทสโทสเตอโรน ( \"เทสโทสเตอโรน\" เป็นฮอร์โมนเพศที่สำคัญที่สุดของเพศชาย เป็นตัวกระตุ้นความต้องการทางเพศให้ตื่นตัวขึ้นนั่นเอง)"
            },
            {
                number: 2,
                title: "โต่วต๋ง",
                image: "assets/imgs/556000008782802.JPEG",
                detail: "โต่วต๋งเป็นชื่อของต้นไม้ที่มีกิ่ง ก้าน เปลือก และใบ ใช้ทำยาบำรุงได้ ซึ่งในเปลือกของต้นโต่วต๋งนั้นจะมียางชนิดหนึ่งที่มีสีขาวเหนียวข้นเป็นเส้นใยรสจืด ส่วนด้านสรรพคุณนั้นมีส่วนช่วยบำรุงไตและตับ, บำรุงกระดูกและเอ็น แก้อักเสบ ปวดกล้ามเนื้อ, ปวดหลัง ปวดบั้นเอว ปวดกระดูก, ข้ออักเสบโดยเฉพาะหัวเข่าและข้อเท้า, ใช้ลดความดันโลหิต เพิ่มระบบภูมิต้านทาน, ใช้บำรุงสตรีมีครรภ์ และสมุนไพรตัวนี้ชาวจีนเชื่อว่าเป็นยาบำรุงสมรรถภาพทางเพศทั้งชายและหญิงได้"
            },
            {
                number: 3,
                title: "เต็กย้ง",
                image: "assets/imgs/556000008782803.JPEG",
                detail: "ต็กย้ง หรือ เขากวางอ่อน เป็นเขาของกวางที่ยังไม่แก่เต็มที่ มีสีน้ำตาลอ่อน รสชาติจืด เขากวางอ่อนที่นิยมใช้ส่วนใหญ่สั่งซื้อมาจากประเทศจีนในราคาคู่ละ 8,000-20,000 บาท การใช้เขากวางอ่อนมีหลายวิธี คือ ต้ม ตุ๋น หรือบดเป็นผงแล้วกินทั้งกาก ปัจจุบันมีการนำมาบดบรรจุแคปซูลขายเม็ดละ 30-40 บาทสรรพคุณนั้นช่วยบำรุงไต, กระดูก, ปอด, กล้ามเนื้อ,เนื้อเยื่อสมอง, ช่วยเรื่องโรคกระดูก, โรคเกี่ยวกับข้อ, รูมาตอยด์ และโรคเก๊า, แก้อ่อนเพลีย,ลดคอเลสเตอรอลและความดันโลหิตสูง ที่สำคัญคือช่วยเสริมฮอร์โมน ช่วยเสริมสมรรถภาพทางเพศชายได้อย่างยอดเยี่ยม เป็นยาอายุวัฒนะ"
            },
            {
                number: 4,
                title: "ตังถังแห่เช่า",
                image: "assets/imgs/556000008782804.JPEG",
                detail: "สมุนไพรจีน ตังถังแห่เช่า หรือเรียกอีกชื่อว่า \"ถั่งเช่า\" หรือหลายคนรู้จักในนามว่า \"ไวอากร้าแห่งเทือกเขาหิมาลัย\" ซึ่งถั่งเช่า แปลเป็นไทยได้ว่า \"ฤดูหนาวเป็นหนอน ฤดูร้อนเป็นหญ้า\" จึงเรียกว่า \"หญ้าหนอน\" เป็นยาสมุนไพรประกอบด้วย 2 ส่วนคือ ส่วนที่เป็นตัวหนอนของผีเสื้อชนิดหนึ่ง มีชื่อวิทยาศาสตร์ว่า Hepialus armoricanus Oberthiir และบนตัวหนอน มีเห็ดชนิดหนึ่งมีชื่อวิทยาศาสตร์ว่า Cordyceps sinensis (Berk.) Saec. เจริญเติบโตอยู่ ถั่งเช่าที่ใช้ทำเป็นยาก็คือ ตัวหนอนและเห็ดที่แห้งแล้ว ซึ่งถั่งเข่ามีรสหวาน ฤทธิ์ปานกลาง กลิ่นหอม เป็นยาบำรุงที่ฤทธิ์ไม่ร้อน (ยาบำรุงส่วนใหญ่ฤทธิ์ค่อนข้างร้อน) ส่วนด้านสรรพคุณนั้นช่วยบำรุงไต, แก้อาการอ่อนเพลีย แก้ไอ ละลายเสมหะ หอบหืด ไอเรื้อรัง, เข่าอ่อน เอวอ่อน, เป็นยาบำรุงสำหรับผู้ป่วยหลังฟื้นไข้และช่วยอาการหย่อนสมรรถภาพทางเพศ"
            },
            {
                number: 5,
                title: "คนจี๋หลิน",
                image: "assets/imgs/556000008782805.JPEG",
                detail: "สมุนไพรตัวนี้ได้สมญานามว่าเป็นราชาแห่งร้อยหญ้า เป็นสมุนไพรทรงคุณค่าตั้งแต่สมัยโบราณกาล มีราคาแพง และใช้เวลาในการเติบโต 40 ปีเป็นอย่างต่ำ มีสรรพคุณบำรุงร่างกาย, กระตุ้นประสาท, ลดน้ำตาลในเลือด, แก้อาการเหนื่อยง่าย"
            },
            {
                number: 6,
                title: "ใบแปะก๊วย ",
                image: "assets/imgs/556000008782806.JPEG",
                detail: "พืชสมุนไพรที่มีต้นกำเนิดจากทางตะวันออกของประเทศจีน (แถบภูเขาด้านตะวันตกของนครเซี่ยงไฮ้) และเป็นมรดกที่สำคัญของทางการแพทย์จีนมาหลายพันปี ในปัจจุบันหลายๆ ประเทศให้การยอมรับถึงสรรพคุณของใบแปะก๊วยในการรักษาโรคสมองเสื่อม โดยนำสารสกัดดังกล่าวมาทำเป็นผลิตภัณฑ์เสริมอาหาร มีสรรพคุณช่วยบำรุงสมอง, โรคความจำเสื่อม อาการหลงๆ ลืมๆ, รักษาโรคซึมเศร้า, ลดความเสี่ยงของโรคมะเร็ง, โรคหัวใจและหลอดเลือดจอประสาทตาเสื่อม, ลดอนุมูลอิสระ, กระตุ้นการไหลเวียนโลหิตและขยายหลอดเลือด ที่สำคัญคือมีคุณสมบัติในการเสริมสมรรถภาพทางเพศได้อีกด้วย"
            },
            {
                number: 7,
                title: "เจียวกู่หลาน",
                image: "assets/imgs/556000008782807.JPEG",
                detail: "เจียวกู่หลานหรือที่บ้านเรานั้นรู้จักและเรียกติดปากกันว่า ปัญจขันธ์ พืชเถาพื้นเมืองในภูมิภาคเขตร้อนและเขตอบอุ่นของเอเชียตะวันออกเฉียงใต้ มีลักษณะเป็นไม้เถาล้มลุก ลำต้นเล็กเรียวยาว เลื้อยยาว แตกกิ่งแขนงได้ โดยส่วนที่ใช้เป็นยา คือต้นส่วนเหนือดินและใบ มีรสชาติค่อนข้างขม หรือขมอมหวาน มีสรรพคุณและมีประโยชน์ต่อร่างกาย และเป็นสมุนไพรที่ชาวจีนให้การขนานนามว่าเป็น \"สมุนไพรอมตะ\" สมุนไพรดังกล่าวที่ว่านี้ปัจจุบันกลายเป็นที่รู้จักอย่างกว้างขวาง ซึ่งสรรพคุณนั้นช่วยลดน้ำตาลในกระแสเลือด, ช่วยให้ความจำดีขึ้น, แก้อาการนอนไม่หลับ, ลดความดันโลหิตสูง, ลดคอเลสเตอรอล, ลดกรดไขมัน, ลดอาการปวดไมเกรน, ลดอาการภูมิแพ้, ป้องกันมะเร็งและต้านอนุมูลอิสระ ที่สำคัญยังสามารถช่วยฟื้นฟูสมรรถภาพทางเพศได้"
            },
            {
                number: 8,
                title: "ติ่มเฮียง",
                image: "assets/imgs/556000008782808.JPEG",
                detail: "สมุนไพรจีนที่มีรสเผ็ดปนขม และมีฤทธิ์อุ่น มีสรรพคุณแก้อาการคลื่นไส้, อาเจียน, อาการท้องอืด ท้องเฟ้อ ปวดท้องกระเพาะ, ป้องกันอาการหอบหืด ช่วยให้ลมปราณไหลเวียนดี และสามารถช่วยเสริมสมรรถภาพทางเพศชายได้ดีอีกด้วย"
            },
            {
                number: 9,
                title: "อิมเอียคัก",
                image: "assets/imgs/556000008782809.JPEG",
                detail: "เป็นสมุนไพรที่มีรสหวาน เผ็ดร้อนและมีฤทธิ์อุ่น มีสรรพคุณบำรุงไต, แก้อาการปวดตามร่างกายต่างๆ อาทิ ปวดหลัง, ปวดเอว, ปวดเข่า และที่สำคัญไปกว่านั้นคือ ช่วยบำรุงสมรรถภาพทางเพศ แก้อาการน้ำกามเคลื่อนที่ (ฝันเปียก) และผู้ที่มีสมรรถภาพเสื่อมถอยได้"
            },
            {
                number: 10,
                title: "โท่วซีจี้",
                image: "assets/imgs/556000008782810.JPEG",
                detail: "เป็นสมุนไพรที่มีรสหวาน มีฤทธิ์อุ่น มีสรรพคุณที่สำคัญสามารถบำรุงไต และรักษากลุ่มอาการไตบกพร่อง (ปวดเอว อวัยวะเพศไม่แข็งตัว ฝันเปียก ปัสสาวะบ่อย ตกขาว) "
            },
        ];
    }
    HomePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        setTimeout(function () {
            _this.isClose = false;
        }, 1000);
        this.loadMap();
    };
    HomePage.prototype.select = function (number) {
        if (number == this.show) {
            this.show = 0;
        }
        else {
            this.show = number;
        }
    };
    HomePage.prototype.openMenu = function () {
        var _this = this;
        if (!this.showMenu) {
            this.loading = true;
            ;
            setTimeout(function () {
                _this.loading = false;
            }, 3000);
        }
        this.showMenu = !this.showMenu;
    };
    HomePage.prototype.loadMap = function () {
        var _this = this;
        this.loading = true;
        Geolocation.getCurrentPosition().then(function (data) {
            var latLng = { lat: data.coords.latitude, lng: data.coords.longitude };
            var mapOptions = {
                center: latLng,
                zoom: 14,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                disableDefaultUI: true,
            };
            _this.map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
            var iconok = {
                url: 'assets/imgs/yin-yang.png',
                scaledSize: new google.maps.Size(30, 30),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(15, 30)
            };
            var marker = new google.maps.Marker({
                map: _this.map,
                position: latLng,
                icon: iconok
            });
            _this.directionsDisplay.setMap(_this.map);
            var service = new google.maps.places.PlacesService(_this.map);
            service.nearbySearch({
                location: latLng,
                radius: 1000,
                type: ['pharmacy']
            }, function (results, status) {
                if (status === google.maps.places.PlacesServiceStatus.OK) {
                    for (var i = 0; i < results.length; i++) {
                        _this.createMarker(results[i], _this.map);
                        // console.log(results[i])
                    }
                }
            });
            _this.loading = false;
        }).catch(function (err) {
        });
    };
    HomePage.prototype.createMarker = function (place, map) {
        var iconok = {
            url: 'assets/imgs/hospital.png',
            scaledSize: new google.maps.Size(30, 30),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(15, 30)
        };
        var placeLoc = place.geometry.location;
        var marker = new google.maps.Marker({
            map: this.map,
            position: placeLoc,
            icon: iconok
        });
        google.maps.event.addListener(marker, 'click', function () {
            // this.navCtrl.push(HomePage)
            // window.open(map, marker);
        });
    };
    HomePage.prototype.close = function () {
        var _this = this;
        if (!this.isClose) {
            this.isClose = true;
            setTimeout(function () {
                _this.navCtrl.pop();
                // this.isClose=false;
            }, 500);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('map'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "mapElement", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/home/moce/Desktop/bon/DCH/src/pages/home/home.html"*/'<div [class]="showMenu?\'header-x active\':\'header-x\'">\n  <div #map id="map"></div>\n  <div class="loader" *ngIf="loading">\n    <img src="assets/imgs/loading.svg" alt="">\n  </div>\n  <div class="btn" (click)="openMenu()">\n    <img src="assets/imgs/down.png" alt="">\n  </div>\n</div>\n<div [class]="showMenu?\'bg-black active\':\'bg-black\'"></div>\n<ion-content class="bg">\n  \n  <div class="constent">\n\n    <div class="list-drug">\n      <div class="bg-red"></div>\n      <div [class]="show==d.number?\'item-x active\':\'item-x\'" *ngFor="let d of data" (click)="select(d.number)">\n        <div class="image">\n          <img src="{{d.image}}" alt="">\n        </div>\n        <div class="button-open">\n          <img src="assets/imgs/arrow-point-to-right.png" alt="">\n        </div>\n        <div class="show">\n          <div class="show-head">\n            <div class="text">{{d.title}}</div>\n          </div>\n          <div class="text-detail">\n            {{d.detail}}\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n\n</ion-content>\n<div [class]="isClose?\'back active\':\'back\'" (click)="close()">\n    <img src="assets/imgs/close.png" alt="">\n  </div>'/*ion-inline-end:"/home/moce/Desktop/bon/DCH/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(222);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(286);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_info_info__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};








var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_info_info__["a" /* InfoPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/info/info.module#InfoPageModule', name: 'InfoPage', segment: 'info', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_info_info__["a" /* InfoPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 286:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_info_info__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_info_info__["a" /* InfoPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/home/moce/Desktop/bon/DCH/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/home/moce/Desktop/bon/DCH/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[199]);
//# sourceMappingURL=main.js.map